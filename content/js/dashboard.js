/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 11.11111111111111, "KoPercent": 88.88888888888889};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.00499767549976755, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.005285412262156448, 500, 1500, "getChildHealthRecords"], "isController": false}, {"data": [0.03260869565217391, 500, 1500, "getClassAttendanceSummaries"], "isController": false}, {"data": [0.0, 500, 1500, "childByID"], "isController": false}, {"data": [0.0, 500, 1500, "getChildStatementOfAccount"], "isController": false}, {"data": [0.0, 500, 1500, "findClassActivities"], "isController": false}, {"data": [0.0, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.0, 500, 1500, "getChildrenToAssignToClass"], "isController": false}, {"data": [0.008316008316008316, 500, 1500, "bankAccountsByFkChild"], "isController": false}, {"data": [0.0, 500, 1500, "getChildrenData"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 4302, 3824, 88.88888888888889, 35369.900976289944, 66, 154868, 10321.5, 104273.6, 152184.85, 153082.64, 13.334697179627856, 7.208016660428619, 47.93674840736074], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getChildHealthRecords", 473, 359, 75.8985200845666, 20500.181818181827, 376, 153799, 9018.0, 51810.8, 86217.39999999988, 152205.76, 1.4940978397177325, 0.7765605849961621, 3.605386486271989], "isController": false}, {"data": ["getClassAttendanceSummaries", 460, 326, 70.8695652173913, 11312.932608695643, 92, 104074, 9004.0, 11928.500000000002, 21351.799999999996, 68254.44999999988, 1.4530200706294105, 0.7997903189379055, 1.5466717548691966], "isController": false}, {"data": ["childByID", 496, 489, 98.58870967741936, 42092.79838709675, 512, 154344, 10400.5, 145030.2, 152410.95, 153758.23, 1.5668534676109906, 0.8781128033804232, 19.946779105250855], "isController": false}, {"data": ["getChildStatementOfAccount", 474, 474, 100.0, 50516.949367088564, 2351, 154868, 11023.5, 151966.0, 152608.75, 153816.75, 1.4974978516908457, 0.7970081339565536, 2.4407460102265834], "isController": false}, {"data": ["findClassActivities", 487, 483, 99.17864476386038, 47492.351129363466, 9002, 154344, 10858.0, 152018.4, 152678.8, 153146.12, 1.5385781894694275, 0.8242806278157244, 5.042449613143944], "isController": false}, {"data": ["getAllClassInfo", 481, 477, 99.16839916839916, 48392.607068607045, 7160, 154688, 10999.0, 152013.2, 152544.4, 153126.54, 1.519353595592927, 0.8254180591442344, 3.9007623074353566], "isController": false}, {"data": ["getChildrenToAssignToClass", 476, 476, 100.0, 48741.65126050417, 3061, 154655, 11048.5, 151975.2, 152611.69999999998, 153286.99000000002, 1.5038069061384387, 0.8003659803178215, 5.97851358875936], "isController": false}, {"data": ["bankAccountsByFkChild", 481, 359, 74.63617463617463, 19695.463617463607, 323, 153745, 9897.0, 51072.2, 82854.29999999996, 152034.32, 1.5195167873434676, 0.829804173656128, 1.948364786896458], "isController": false}, {"data": ["getChildrenData", 474, 381, 80.37974683544304, 28180.493670886077, 66, 152433, 10854.5, 86821.0, 96552.75, 145226.5, 1.4692571882013068, 0.7994316702416524, 4.361857277472629], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["502/Bad Gateway", 3824, 100.0, 88.88888888888889], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 4302, 3824, "502/Bad Gateway", 3824, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["getChildHealthRecords", 473, 359, "502/Bad Gateway", 359, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getClassAttendanceSummaries", 460, 326, "502/Bad Gateway", 326, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["childByID", 496, 489, "502/Bad Gateway", 489, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildStatementOfAccount", 474, 474, "502/Bad Gateway", 474, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findClassActivities", 487, 483, "502/Bad Gateway", 483, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getAllClassInfo", 481, 477, "502/Bad Gateway", 477, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildrenToAssignToClass", 476, 476, "502/Bad Gateway", 476, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["bankAccountsByFkChild", 481, 359, "502/Bad Gateway", 359, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildrenData", 474, 381, "502/Bad Gateway", 381, null, null, null, null, null, null, null, null], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
